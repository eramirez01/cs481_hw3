﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Net.Sockets;

namespace CS481_HW3_Hierarchy
{
    public class DoggyPage
    {
        public string dogName
        {
            get;
            set;
        }

        public string dogDescription
        {
            get;
            set;
        }

        public ImageSource dogImage
        {
            get;
            set;
        }


    }

    public class CattPage
    {
        public string catName
        {
            get;
            set;
        }

        public string catDescription
        {
            get;
            set;
        }

        public ImageSource catImage
        {
            get;
            set;
        }
    }
}
