﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW3_Hierarchy
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<DoggyPage> dogDes = new ObservableCollection<DoggyPage>();

        ObservableCollection<CattPage> catDes = new ObservableCollection<CattPage>();

        public MainPage()
        {
            InitializeComponent();

            var dog1 = new DoggyPage
            {
                dogName = "Spot",
                dogImage = "https://d17fnq9dkz9hgj.cloudfront.net/breed-uploads/2018/09/dog-landing-hero-lg.jpg?bust=1536935129&width=1080",
                dogDescription = "The spot is on his eye. Get it?",
            };

            dogDes.Add(dog1);

            Title_Label.Text = dogDes[0].dogName;
            dogImage.Source = dogDes[0].dogImage;


        }

        private async void InfoButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DogPic());
        }

        private async void NextButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Next());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }


    }
}
