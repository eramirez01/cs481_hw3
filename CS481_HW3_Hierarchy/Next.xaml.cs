﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Next : ContentPage
    {
        public Next()
        {
        }

        public Next(ObservableCollection<CattPage> list)
        {
            InitializeComponent();
            Title_Label.Text = list[0].catName;
            catImage.Source = list[0].catImage;
            catDescription.Text = list[0].catDescription;
        }


    }
}