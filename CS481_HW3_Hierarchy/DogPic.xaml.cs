﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{

    public partial class DogPic : ContentPage
    {
        //[XamlCompilation(XamlCompilationOptions.Compile)]
        public DogPic()
        {
        }

        public DogPic(ObservableCollection<DoggyPage> list)
        {
            InitializeComponent();
            Title_Label.Text = list[0].dogName;
            dogImage.Source = list[0].dogImage;
            dogDescription.Text = list[0].dogDescription;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}